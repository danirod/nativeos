#include <stdlib.h>

void *
calloc (size_t count, size_t size)
{
	size_t total;
	void * region;
	unsigned char * buffer;

	total = count * size;
	region = malloc(total);
	if (!region) {
		/* TODO: ENOMEM -> errno.  */
		return 0;
	}

	/* Clean up the memory region before returning.  */
	buffer = (unsigned char *) region;
	while ((void *) buffer - region <= total) {
		*buffer++ = 0;
	}
	return region;
}
