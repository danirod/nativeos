#pragma once

#include <stddef.h>

/**
 * @brief Allocate some memory buffer in the heap.
 * @param size the amount of bytes to allocate in the heap.
 * @return a pointer to the allocated buffer or NULL if there was an error.
 */
void * malloc (size_t size);

/**
 * @brief Contiguosly allocates space for an amount of objects of a size.
 * @param count the amount of objects that have to be allocated.
 * @param size the size of each object to be allocated.
 * @return a memory pointer to the allocated memory region.
 */
void * calloc (size_t count, size_t size);

/**
 * @brief Deallocate a memory buffer from the heap.
 * @param ptr a pointer to the memory region to be deallocated from heap.
 */
void free (void * ptr);
